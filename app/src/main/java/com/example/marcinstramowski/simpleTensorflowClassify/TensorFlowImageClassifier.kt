package com.example.marcinstramowski.simpleTensorflowClassify

import android.content.res.AssetManager
import android.graphics.Bitmap
import org.tensorflow.contrib.android.TensorFlowInferenceInterface

class TensorFlowImageClassifier constructor(
    assetManager: AssetManager,
    modelFilename: String,
    labelFilename: String
) {

    companion object {

        /**
         * Neural network image input size
         */
        const val INPUT_SIZE = 128

        /**
         * Input layer name of neural network
         */
        private const val INPUT_NAME = "input"

        /**
         * Output layer name of neural network
         */
        private const val OUTPUT_NAME = "final_result"

        /**
         * Input mean of neural network
         */
        private const val IMAGE_MEAN = 128

        /**
         * Input std of neural network
         */
        private const val IMAGE_STD = 128f

        /**
         * Input image size to the neural network
         */
        private const val NUMBER_OF_IMAGES = 1L

        /**
         * Number of channels of input image
         */
        private const val NUMBER_OF_CHANNELS = 3

        /**
         * [Recognition] objects returned by [recogniseObjects]
         * with [Recognition.confidence] below passed [CONFIDENCE_THRESHOLD] will be rejected
         */
        private const val CONFIDENCE_THRESHOLD = 0.1f

        /**
         * Defines maximum number of returned objects by [recogniseObjects] function
         */
        private const val MAX_RESULTS = 3
    }

    /**
     * Output layer labels
     */
    private val labels = FileUtils.loadLabels(assetManager, labelFilename)

    /**
     * Each value is a packed int representing a Color.
     * More info: https://developer.android.com/reference/android/graphics/Color.html
     */
    private val colorIntValues = IntArray(INPUT_SIZE * INPUT_SIZE)

    /**
     * [colorIntValues] converted to match neural network input
     *
     * For example MobileNet models require r, g, b colors in range of <-1, 1>
     */
    private val colorFloatValues = FloatArray(INPUT_SIZE * INPUT_SIZE * NUMBER_OF_CHANNELS)

    /**
     * Output layer confidence values
     */
    private val outputs: FloatArray

    private val inferenceInterface = TensorFlowInferenceInterface(assetManager, modelFilename)

    init {
        val numOutputs = inferenceInterface.graphOperation(OUTPUT_NAME).output<Any>(0).shape().size(1).toInt()
        outputs = FloatArray(numOutputs)
    }

    /**
     * Provides passed [bitmap] data to configured neural network and returns list of [Recognition] objects.
     */
    fun recogniseObjects(bitmap: Bitmap): List<Recognition> {

        bitmap.getPixels(colorIntValues, 0, bitmap.width, 0, 0, bitmap.width, bitmap.height)

        colorIntValues.forEachIndexed { i, intValue ->
            colorFloatValues[i * 3 + 0] = ((intValue shr 16 and 0xFF) - IMAGE_MEAN) / IMAGE_STD // red channel
            colorFloatValues[i * 3 + 1] = ((intValue shr 8 and 0xFF) - IMAGE_MEAN) / IMAGE_STD // green channel
            colorFloatValues[i * 3 + 2] = ((intValue and 0xFF) - IMAGE_MEAN) / IMAGE_STD // blue channel
        }

        // Configure TensorFlowInferenceInterface input
        inferenceInterface.feed(INPUT_NAME, colorFloatValues, NUMBER_OF_IMAGES,
                INPUT_SIZE.toLong(), INPUT_SIZE.toLong(), NUMBER_OF_CHANNELS.toLong())
        // A list of output nodes which should be filled by the inference pass.
        inferenceInterface.run(arrayOf(OUTPUT_NAME))
        // Read from a Tensor named [OUTPUT_NAME] and copy the contents into [outputs] array
        inferenceInterface.fetch(OUTPUT_NAME, outputs)

        return outputs
            .mapIndexed { index, confidence -> // map returned confidence with label names
                Recognition(labels.getOrNull(index) ?: "unknown", confidence)
            }
            .filter { it.confidence > CONFIDENCE_THRESHOLD } // filter recognised objects
            .sortedByDescending { it.confidence } // sort from highest confidence
            .take(MAX_RESULTS) // take first X results
            .toList() // return as list
    }
}

