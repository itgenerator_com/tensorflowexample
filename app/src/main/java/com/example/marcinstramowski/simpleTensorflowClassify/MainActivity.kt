package com.example.marcinstramowski.simpleTensorflowClassify

import android.Manifest
import android.content.pm.PackageManager
import android.os.Bundle
import android.support.v4.app.ActivityCompat
import android.support.v4.content.ContextCompat
import android.support.v7.app.AppCompatActivity
import com.example.marcinstramowski.simpleTensorflowClassify.ImageUtils.Companion.prepareBitmap
import io.fotoapparat.Fotoapparat
import io.fotoapparat.configuration.CameraConfiguration
import io.fotoapparat.parameter.Resolution
import io.fotoapparat.preview.Frame
import io.fotoapparat.selector.*
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {

    companion object {

        /**
         * Preferred camera preview size in pixels
         */
        private const val PREFERRED_CAMERA_PREVIEW_AREA = 640 * 480

        /**
         * Model filename placed in assets
         */
        private const val MODEL_FILE_NAME = "tensorflow_model.pb"

        /**
         * Labels filename placed in assets
         */
        private const val LABEL_FILE_NAME = "tensorflow_labels.txt"
    }

    private lateinit var tensorFlowClassifier: TensorFlowImageClassifier
    private lateinit var camera: Fotoapparat

    private val cameraConfiguration = CameraConfiguration(
        previewResolution = standardRatio(
            firstAvailable(selectBestResolutionWithMaxAreaOf(PREFERRED_CAMERA_PREVIEW_AREA), lowestResolution())
        ),
        previewFpsRange = highestFps(),
        focusMode = firstAvailable(continuousFocusPicture(), autoFocus()),
        frameProcessor = { frameToProcess -> processFrame(frameToProcess) }
    )

    /**
     * Filter available resolutions with bigger area then [maxPreviewArea] and then picks highest.
     */
    private fun selectBestResolutionWithMaxAreaOf(maxPreviewArea: Int): Iterable<Resolution>.() -> Resolution? = {
        toList().filter { it.area <= maxPreviewArea }.maxBy { it.area }
    }

    private fun processFrame(frame: Frame) {
        val preparedBitmap = prepareBitmap(
            applicationContext,
            frame.image,
            frame.size.width,
            frame.size.height,
            frame.rotation,
            TensorFlowImageClassifier.INPUT_SIZE
        )
        runOnUiThread { tensorflow_input.setImageBitmap(preparedBitmap) } // show neural network input image
        val results = tensorFlowClassifier.recogniseObjects(preparedBitmap)
        showResults(results)
    }

    private fun showResults(results: List<Recognition>) {

        fun Float.toPercent(): Int = this.times(100).toInt()

        val resultsString = results
            .joinToString(separator = "\n") { "${it.title.capitalize()}: ${it.confidence.toPercent()}%" }
            .trimEnd('\n')

        this.runOnUiThread { results_list.text = resultsString }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        initializeCameraWithPermissions()
        initializeClassifier()
    }

    /**
     * Checks for Camera permissions. If it's granted - initializes camera otherwise closes the app
     */
    private fun initializeCameraWithPermissions() {
        val permission = Manifest.permission.CAMERA
        if (ContextCompat.checkSelfPermission(this, permission) == PackageManager.PERMISSION_GRANTED){
            camera = Fotoapparat(this, camera_view, cameraConfiguration = cameraConfiguration)
        } else {
            ActivityCompat.requestPermissions(this, arrayOf(permission), 1234)
            finish()
        }
    }

    private fun initializeClassifier() {
        tensorFlowClassifier =
                TensorFlowImageClassifier(assets, MODEL_FILE_NAME, LABEL_FILE_NAME)
    }

    override fun onResume() {
        super.onResume()
        camera.start()
    }

    override fun onPause() {
        camera.stop()
        super.onPause()
    }
}
