package com.example.marcinstramowski.simpleTensorflowClassify

data class Recognition(val title: String, val confidence: Float)