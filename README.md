# SimpleTensorflowClassify

Tensorflow classifier wrote in Kotlin. It uses renderscript instead of NDK to process YUV image frames, so you don't need to bother with additional C++ configuration. 

## Description

This example uses the retrained MobileNet model to classify camera frames in real-time, displaying the top results in an overlay on the camera image.
